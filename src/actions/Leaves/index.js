import {FetchMethod} from '../../common/utils';
const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/";

export const save_leave = (data) => {
    return FetchMethod({
        url: host+"leave_types",
        sendData : data,
        isAuth : true
    });
}

export const get_leaves = (filter) => {
    const {searchBy,searchWords,sortBy,sortType} =  filter;
    var url = new URL(host+"leave_types"),params = {searchBy:searchBy,searchWords:searchWords,sortBy:sortBy,sortType:sortType}
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
    return FetchMethod({
        url : url,
        method : "GET",
        isAuth : true
    })
}

export const update_leave = (data) => {
    return FetchMethod({
        url: host+"leave_types/"+ data.id,
        method : "PUT",
        sendData : data,
        isAuth : true
    })
}

export const del_leave = (id) => {
    return FetchMethod({
        url: host+"leave_types/"+ id,
        method: "DELETE",
        isAuth : true
    })
}