export const getPost = () => {
    return dispatch => {
        return fetch("http://localhost:5000/api/posts/5e16f4380c9304037c0bc904",{
            method : "GET",
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'auth-token':localStorage.getItem('usertoken')
            },
        })
        .then((res) => res.json())
        .then((data)=>{
            return data;
        })
        .catch((error) => {
            return error;
        })
    }
}

