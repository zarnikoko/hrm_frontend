import {FetchMethod} from '../../common/utils';
const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/";

export const get_noti = (param) => {
    const {notiPage,loadStatus,dateTime} = param;
    return FetchMethod({
        url: host+"get_noti?page="+notiPage+"&status="+loadStatus+"&dateTime="+dateTime,
        isAuth : true,
        method:"GET"
    });
}

export const get_noti_by_id = (id) => {
    return FetchMethod({
        url: host+"get_noti_by_id/"+id,
        isAuth : true,
        method:"GET"
    });
}

export const all_mark_as_read = () => {
    return FetchMethod({
        url: host+"all_mark_as_read",
        isAuth : true,
        method:"PUT"
    });
}

export const delete_all_read = () => {
    return FetchMethod({
        url: host+"delete_all_read",
        isAuth : true,
        method:"PUT"
    });
}

export const mark_as_read = (id) => {
    return FetchMethod({
        url: host+"mark_as_read/"+id,
        isAuth : true,
        method:"PUT"
    });
}

export const delete_read = (id) => {
    return FetchMethod({
        url: host+"delete_read/"+id,
        isAuth : true,
        method:"DELETE"
    });
}
