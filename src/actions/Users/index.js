import {FetchMethod} from '../../common/utils';
const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/";

export const get_user = (filter)=> {
    const {page,limit,searchBy,searchWords,sortBy,sortType} =  filter;
    var url = new URL(host+"user"),params = {page:page,limit:limit,searchBy:searchBy,searchWords:searchWords,sortBy:sortBy,sortType:sortType}
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
    return FetchMethod({
        url:url,
        method : "GET",
        isAuth : true
    })
}

export const store_user = (data) => {
    return FetchMethod({
        url:host+"user",
        isAuth : true,
        sendData : data
    })
}

export const update_user = (data) => {
    const {id} = data;
    return FetchMethod({
        url:host+"user/"+id,
        isAuth : true,
        method : "PATCH",
        sendData : data
    })
}

export const delete_user = (id) => {
    return FetchMethod({
        url:host+"user/"+id,
        isAuth : true,
        method : "DELETE"
    })
}