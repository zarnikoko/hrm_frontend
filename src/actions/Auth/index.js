import {FetchMethod} from '../../common/utils';

const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/";

export const login = (data) => {
    return FetchMethod({
        url : host+"login",
        sendData : data
    });
}

export const logout = ()=>{
    return FetchMethod({
        url : host+"logout",
        isAuth : true
    })
}