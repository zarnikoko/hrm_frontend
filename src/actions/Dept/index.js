import {FetchMethod} from '../../common/utils';
const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT+"api/";

export const create_dept = (data) => {
    return FetchMethod({
        url: host+"department",
        sendData : data,
        isAuth : true
    });
}

export const get_dept = (data) => {
    const {searchBy,searchWords,sortBy,sortType} = data;
    var url = new URL(host+"department"),params = {searchBy:searchBy,searchWords:searchWords,sortBy:sortBy,sortType:sortType}
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
    return FetchMethod({
        url: url,
        isAuth : true,
        method:"GET",
    });
}

export const update_dept = (data) => {
    const {id} = data
    return FetchMethod({
        url: host+"department/"+id,
        isAuth : true,
        method:"PATCH",
        sendData:data
    });
}

export const delete_dept = (id) => {
    return FetchMethod({
        url: host+"department/"+id,
        isAuth : true,
        method:"DELETE"
    })
}