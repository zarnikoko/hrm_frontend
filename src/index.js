import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import {createStore,applyMiddleware,compose} from 'redux';
import {Provider} from 'react-redux';
import {reducer} from './reducers'
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/root_saga';
import Echo from 'laravel-echo';
import {Provider as AlertProvider,positions,transitions} from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

const host = "http://"+window.location.hostname+process.env.REACT_APP_API_PORT;

window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.REACT_APP_WS_KEY,
    wsHost: window.location.hostname,
    wsPort: process.env.REACT_APP_WS_PORT,
    disableStats: true,
    authEndpoint:host+'broadcasting/auth',
    auth: {
        headers: {
        Authorization: 'Bearer '+localStorage.getItem('usertoken'),
        Accept: 'application/json',
        },
    },
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
    reducer,
    composeEnhancer(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

const alertOptions = {
    offset: '60px',
    timeout: 3000,
    transition: transitions.SCALE,
    containerStyle: {
        zIndex: '9999999',
    },
}

// For todays date;
Date.prototype.today = function () { 
    return ( this.getFullYear()+"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+((this.getDate() < 10)?"0":"") + this.getDate());
    //(this.getDate() < 10)?"0":"") + this.getDate()
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

ReactDOM.render(
<Provider store = {store}>
    <BrowserRouter>
        <AlertProvider template={AlertTemplate}>
            <AlertProvider
                template={AlertTemplate}
                position={positions.MIDDLE} //default position for all alerts without individual position
                {...alertOptions}
            >
                <App />
            </AlertProvider>
        </AlertProvider>
    </BrowserRouter>
</Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
