import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {useSelector} from 'react-redux';
import { isLogin } from '../common/utils';

export const PublicRoute = ({component: Component, restricted, ...rest}) => {
    return (
        // restricted = false meaning public route
        // restricted = true meaning restricted route
        <Route {...rest} render={props => (
            isLogin() && restricted ?
                <Redirect to="/dashboard" />
            : <Component {...props} key={Component} />
        )} />
    );
};

export const PrivateRoute = ({component: Component, user_type, ...rest}) => {
    const user = useSelector(state => state.currentUser);
    return (
        // Show the component only when the user is logged in
        // Otherwise, redirect the user to /signin page
        <Route {...rest} render={props => (
            isLogin()?[
                (user.type===user_type)?
                <Component {...props} key={Component}/> 
                :<Redirect to="/404"/>
            ]
            : <Redirect to="/login" />
        )} />
    );
};

