import React,{useEffect,useState,useRef} from 'react'
import SimpleReactValidator from 'simple-react-validator';
import {toShortForm} from '../../../common/utils';

const useForceUpdate = () => {
    const [value,setValue] = useState(0); // integer state
    return () => setValue(value => ++value); // update the state to force render
}

const SetLeaveTypeForm = (props)=>{
        const validator = useRef(new SimpleReactValidator());
        const forceUpdate = useForceUpdate();
        const {parentprops} = props;
        const {Update_Object,Create_Leave,data} = parentprops;
        const {editLeaves} = data;
        const didMountRef = useRef(false);
        const prevNameRef = useRef();

        useEffect(() => {
            prevNameRef.current = editLeaves.name;
        });

        const prevName = prevNameRef.current;
        
        const handleInputChanged = (e) => {
            e.persist();
            validator.current.showMessageFor(e.target.name);
            Update_Object({
                name:"editLeaves",
                value : {
                   [e.target.name] : e.target.value
                }
            })
        }

        const handleInputBlured = (e) => {
            e.persist();
            validator.current.showMessageFor('name');
            if(e.target.value !== '' && editLeaves.id === undefined) {
                if(e.target.value !== '') {
                    Update_Object({
                        name:"editLeaves",
                        value : {
                        code : toShortForm(e.target.value)
                        }
                    })
                }
            }
        }

        const handleSubmit = (e) => {
            e.preventDefault();
            if (validator.current.allValid()) {
                Create_Leave(parentprops);
              } else {
                validator.current.showMessages();
                // rerender to show messages for the first time
                // you can use the autoForceUpdate option to do this automatically`
                forceUpdate();
            }   
        }
        
        useEffect(()=>{
            if(!didMountRef.current){
               didMountRef.current = true;
            }else{
                if(prevName!==editLeaves.name){
                    if(editLeaves.name===''){
                        Update_Object({
                            name:"editLeaves",
                            value : {
                               "code" : ""
                            }
                        })
                    }
                }
            }
        },[editLeaves.name]);

        return(
            <div style={{maxHeight:"365px",overflowY:"auto",overflowX:"hidden"}}>
                <form id="leaveForm" noValidate onSubmit={(e)=>handleSubmit(e)}>
                    <div className="form-group row required">
                        <label htmlFor="name" className="col-sm-2 col-form-label control-label">Name</label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                name="name"
                                placeholder="Name"
                                value={editLeaves.name}
                                onChange = {(e)=>handleInputChanged(e)}
                                onBlur = {(e)=>handleInputBlured(e)}
                            />
                            <span className="text-danger">{validator.current.message('name', editLeaves.name, 'required|max:255')}</span>
                        </div>
                    </div>
                    <div className="form-group row required">
                        <label htmlFor="code" className="col-sm-2 col-form-label control-label">Code</label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                name="code"
                                placeholder="Code"
                                value={(editLeaves.name==='')?'':editLeaves.code}
                                readOnly = {(editLeaves.name==='')?true:false}
                                onChange = {(e)=>handleInputChanged(e)}
                                onBlur = {()=>validator.current.showMessageFor('code')}
                            />
                            <span className="text-danger">{validator.current.message('code', editLeaves.code, 'required|max:255')}</span>
                        </div>
                    </div>
                    <div className="form-group row required">
                        <label htmlFor="type" className="col-sm-2 col-form-label control-label">Type</label>   
                        <div className="col">         
                            <select 
                                className="form-control col-sm-5" 
                                onChange={(e)=>handleInputChanged(e)} 
                                onBlur = {()=>validator.current.showMessageFor('type')}
                                value={editLeaves.type} 
                                name={'type'}
                            >
                                <option value="">Select type</option>
                                <option value="paid">Paid</option>
                                <option value="unpaid">Unpaid</option>
                            </select>
                            <span className="text-danger">{validator.current.message('type', editLeaves.type, 'required')}</span>
                        </div>
                    </div>
                    <fieldset className="form-group required">
                        <div className="row">
                            <legend className="col-form-label col-sm-2 pt-0 control-label">Units</legend>
                            <div className="col-sm-10">
                                <div className="form-check-inline">
                                    <input 
                                    className="form-check-input" 
                                    type="radio" 
                                    name="unit" 
                                    value="days"
                                    onChange={(e)=>handleInputChanged(e)} 
                                    checked = {(editLeaves.unit==='days')?true:false}
                                    />
                                    <label className="form-check-label" htmlFor="gridRadios1">
                                        Days
                                    </label>
                                </div>
                                <div className="form-check-inline">
                                    <input 
                                    className="form-check-input" 
                                    type="radio" 
                                    name="unit" 
                                    value="hours"
                                    onChange={(e)=>handleInputChanged(e)} 
                                    checked = {(editLeaves.unit==='hours')?true:false}
                                    />
                                    <label className="form-check-label" htmlFor="gridRadios2">
                                        Hours
                                    </label>
                                </div>
                                <span className="text-danger">{validator.current.message('unit', editLeaves.unit, 'required')}</span>
                            </div>
                        </div>
                    </fieldset>
                    <div className="form-group row required">
                        <label htmlFor="amount" className="col-sm-2 col-form-label control-label">Amount</label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                name="amount"
                                placeholder="Amount"
                                value={editLeaves.amount}
                                onChange = {(e)=>handleInputChanged(e)}
                                onBlur = {()=>validator.current.showMessageFor('amount')}
                            />
                            <span className="text-danger">{validator.current.message('amount',editLeaves.amount, 'required|numeric|min:1,num')}</span>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="description" className="col-sm-2 col-form-label">Description</label>
                        <div className="col-sm-8">
                            <textarea 
                            className="form-control" 
                            name="description"
                            value={(editLeaves.description)?editLeaves.description:""}
                            onChange={(e)=>handleInputChanged(e)}
                            rows="3"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        )
}

export default SetLeaveTypeForm