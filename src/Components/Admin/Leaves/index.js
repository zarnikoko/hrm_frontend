import React,{useEffect,useRef} from 'react';
import {connect} from 'react-redux';
import ModalFrame from '../../../common/ModalFrame';
import LeaveForm from './LeaveForm';
import LeaveTable from './LeaveTable';
import SearchBy from '../../System/RegisterUser/SearchBy';
import ActionBtn from '../../../common/ActionBtn';
import ResetBtn from '../../../common/ResetBtn';

const Leaves = (props) => {
    const {Update_Multiple,Update_Object,data,Get_Leave,Create,Destroy,Leave_Modal_Hide} = props;
    const {title='save',isShowModal=false,isDetailModal=false,savingLoading=false,editLeaves=[],leave={},searchFilter=initialData.searchFilter,resetEditLeaves={}} = data||{};
    const {searchBy,searchWords,sortBy,sortType} = searchFilter;
    let header = (title==='save')?'New Leave' : 'Edit Leave';
    const didMountRef = useRef(false);
    const prevSearchByRef = useRef();
    const prevSearchWordsRef = useRef();
    
    useEffect(()=>{
        if(!didMountRef.current){
            Create(); // document.ready
            Get_Leave(props);
        }
        return() =>{
            Destroy();
        }
    },[]);

    useEffect(() => {
        prevSearchByRef.current = searchBy;
        prevSearchWordsRef.current = searchWords;
    });

    const prevSearchBy = prevSearchByRef.current;
    const prevSearchWords =  prevSearchWordsRef.current;

    useEffect(()=>{
        if(!didMountRef.current){
           didMountRef.current = 1;
        }else{
            if(prevSearchWords !== searchWords){
                if(searchWords.slice(-1).split(" ").length - 1 === 0){
                    const timeout = setTimeout(() => {
                        Get_Leave(props);
                    },500)
                    return () => {
                        clearTimeout(timeout);
                    } 
                }
            }else if(prevSearchBy !== searchBy){
                if(searchWords !== ''){
                    Get_Leave(props);
                }
            }else{
                Get_Leave(props);
            }
        }// eslint-disable-next-line
    },[searchWords,searchBy,sortBy,sortType]);

    const reset = () => {
        Update_Multiple({
            "editLeaves":resetEditLeaves
        });
    }

    const SetDetailForm = () => {
        return (
            <div style={{maxHeight:"365px",overflowY:"auto",overflowX:"hidden",whiteSpace:"pre-line"}}>
                <p>{leave.description}</p>
            </div>
        )
    }

    const handleSearch = (e) => {
        e.persist();
        if(searchWords==='' && e.target.name === 'searchBy'){
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                }
            });
        }else{
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                    page : 1
                }
            });
        }
    }

    return (
        <div className="container-fluid vh-100" style={{paddingTop:"60px",overflowY:"hidden"}}>
            <div className="row justify-content-between">
                <div className="col">
                    <button 
                    className="btn btn-outline-primary"
                    onClick = {()=>Update_Multiple({
                        "isShowModal":true,
                        "title": "save"
                    })}>
                        <i className="fa fa-plus-circle" aria-hidden="true"></i>
                    </button>
                </div>
                <div className="col">
                    <div className="form-inline">
                        <label htmlFor="searchInput" className="mr-3">Search By</label>
                        <SearchBy options = {['name','code','type','unit','amount']} selected={searchBy} onChange={handleSearch} name="searchBy"/>
                        <input 
                        type="text"
                        value ={searchWords}
                        onChange = {handleSearch} 
                        className="form-control ml-3" 
                        name="searchWords" 
                        placeholder="Search..."
                        />
                    </div>
                </div>
            </div>
            <div className="row mt-3">
                <div>
                    <LeaveTable parentprops = {props} />
                </div>
            </div>
            <ModalFrame
                size='md'
                backdrop={(savingLoading)?'static':undefined}
                show={isShowModal}
                loading = {savingLoading?1:0}
                onHide={() => Leave_Modal_Hide('main')}
                header={header}
                form={<LeaveForm parentprops={props}/>}
                actionbtn = {<ActionBtn form="leaveForm" loading={savingLoading} title={title}/>}
                resetbtn = {title==='edit'?<ResetBtn reset={reset} loading={savingLoading}/>:undefined}
            />
            <ModalFrame
                size='md'
                backdrop={(savingLoading)?'static':undefined}
                show={isDetailModal}
                loading = {savingLoading?1:0}
                onHide={() => Leave_Modal_Hide('description')}
                header={'Description of '+leave.name}
                form={<SetDetailForm />}
            />
        </div>
    )
}

export const initialData = {
    isShowModal : false,
    isDetailModal : false,
    savingLoading : false,
    leaves : [],
    editLeaves :{
        name:'',
        amount:'',
        code : '',
        type:'',
        unit:'',
        description:'',
    },
    searchFilter : {
        searchBy : 'Name',
        searchWords : '',
        sortBy : "updated_at",
        sortType : "desc",
    },
}

const mapStatetoProps = state =>({
    isLoading : state.isLoading,
    initialData : initialData,
    data : state.data,
})

const mapDispatchtoProps = dispatch =>({ 
    Create : () => {dispatch({
        type : "CREATE",
        data : initialData
    })},
    Update : (e) => {dispatch({
        type : "UPDATE",
        name : e.name,
        value: e.value
    })},
    Update_Object : (e) =>{dispatch({
        type: "UPDATE_OBJECT",
        name :e.name,
        value : e.value
    })},
    Update_Multiple : (obj) => {dispatch({
        type : "UPDATE_MULTIPLE",
        obj : obj
    })},
    Destroy : () => {dispatch({
        type : "DESTROY",
    })},
    Create_Leave :()=>{dispatch({
        type:"CREATE_LEAVE",
    })},
    Get_Leave :()=> {dispatch({
        type:"GET_LEAVE",
    })},
    Delete_Leave :(p)=>dispatch({
        type:"DELETE_LEAVE",
        ...p
    }),
    Leave_Modal_Hide : (name) => dispatch({
        type : "LEAVE_MODAL_HIDE",
        name : name
    }),
    // setLeaveStore : (val) => {dispatch({type:"SET_LEAVE_STORE",set:val})},
    // showModal : (status) => {dispatch({type:"SHOW_MODAL",status:status})},
    // showModalDesc : (status) => {dispatch({type:"SHOW_MODAL",status:status})},
    // setRegisterLoading : (status) => {dispatch({type:"SET_REGISTER_LOADING",status:status})},
    // inputChange : (e)=> {dispatch({type:"INPUT_CHANGE",event:e})},
    // setLeave :(data)=>{dispatch({type:"SET_LEAVE",data:data})},
    // getLeave : ()=> {dispatch({type:"GET_LEAVE"})},
    // saveStore : (e) => {dispatch({type:"SAVE_STORE",name:e.name,value:e.value})},
    // clickDetail : (obj) => {dispatch({type:"CLICK_LEAVE_DEC_DETAIL",modalState:obj.modalStatus,value:obj.value})},
    // editModal : (e) => {dispatch({type:"EDIT_MODAL",name:e.name,value:e.val})},
    // editForm : (data) => {dispatch({type:"EDIT_FORM",modalState:data.modalState,value:data.value})},
    // editInputChange : (e) => {dispatch({type:"EDIT_INPUT_CHANGE",name:e.name,value:e.value})}
})

export default connect(mapStatetoProps,mapDispatchtoProps)(Leaves)