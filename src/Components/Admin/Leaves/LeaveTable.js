import React from 'react'
import {longTextCutter} from '../../../common/utils';
import SortColumn from '../../../common/SortColumn';
import '../../System/RegisterUser/UserTable.css';

const LeaveTable = (props) => {
    const {parentprops} = props;
    const {isLoading,Update_Multiple,Delete_Leave,Get_Leave} = parentprops;
    const {leaves=[]} =  props.parentprops.data||{};
    const refresh = () => {
        Get_Leave(props.parentprops);
    }
    const Rows = leaves.map((val)=>{ // 
        return  <tr key={val.id}>
            <td>{val.name}</td>
            <td className="text-center">{val.code}</td>
            <td className="text-center">{val.type}</td>
            <td className="text-center">{val.unit}</td>
            <td className="text-center">{val.amount}</td>
            <td style={{overflow:"hidden"}}>
                {
                (val['description']!== null && val['description'].length>10)?
                    <span>
                        {longTextCutter(val['description'])}<br/>
                        <button 
                            className="btn btn-link" 
                            style={{fontSize: 10}}
                            onClick={()=> Update_Multiple({
                                "isDetailModal":true,
                                "leave" :val,
                                "title" : "edit"
                            })}
                        >
                            Show Detail
                        </button>
                    </span>
                :val['description']
                }
            </td>
            <td className="text-center">
                <button 
                    className="btn btn-outline-primary mr-3"
                    title ="Edit"
                    onClick={()=> Update_Multiple({
                        "isShowModal":true,
                        "editLeaves" :val,
                        "resetEditLeaves" :val,
                        "title" : "edit"
                    })}
                >
                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </button>
                <button 
                    className="btn btn-outline-danger mr-3"
                    title ="Delete"
                    onClick={() => Delete_Leave({
                        "editLeaves" :val,
                        "title" : "delete"
                    })}
                >
                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                </button>
            </td>
        </tr>;
    })

    return(
        <table className="table table-hover" style={{tableLayout:"fixed"}}>
            <thead>
                <tr>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Name" showData = {leaves} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Code" showData = {leaves} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Type" showData = {leaves} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Unit" showData = {leaves} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Amount" showData = {leaves} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                       Description
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                       Action
                    </th>
                </tr>
            </thead>
            <tbody style={{height:"400px"}}>
                {(isLoading)?
                    <tr style={{height:"100%"}}>
                        <td colSpan="4" align="center" style={{verticalAlign:"middle"}}>
                            <div className="spinner-border text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                            <div><h6>&nbsp;&nbsp;&nbsp;Loading ... </h6></div>
                        </td>
                    </tr>
                    :(leaves.length>0)? Rows
                    :
                    <tr style={{height:"100%"}} onClick={()=>refresh()}>
                        <td colSpan="4" align="center" style={{verticalAlign:"middle"}}>
                            <span className="text-muted"> No data to show ... click to refresh </span>
                        </td>
                    </tr>
                }
            </tbody>
        </table>
    )
}

export default LeaveTable