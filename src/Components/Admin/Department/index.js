import React,{useEffect,useRef} from 'react';
import DeptModal from './DeptModal';
import DeptTable from './DeptTable';
import SearchBy from '../../System/RegisterUser/SearchBy';
import {connect} from 'react-redux';

const Department = (props) => {
    const {Create,Destroy,Update_Multiple,Get_Dept,data,Update_Object} = props;
    const {isShowModal=false,searchFilter=initialData.searchFilter}  = data||{};
    const {searchBy,searchWords,sortBy,sortType} = searchFilter;
    const didMountRef = useRef(false);
    const prevSearchByRef = useRef();
    const prevSearchWordsRef = useRef();

    const handleOnHide = () => {
        Update_Multiple({
            isShowModal:false,
        });
        setTimeout(()=>{
            Update_Multiple({
                editDept:initialData.editDept,
                resetDept:undefined
            });
        },100)
    }

    const handleOnShow = () => {
        Update_Multiple({
            isShowModal:true,
            title:"save"
        });
    }

    useEffect(() => {
        prevSearchByRef.current = searchBy;
        prevSearchWordsRef.current = searchWords;
    });

    const prevSearchBy = prevSearchByRef.current;
    const prevSearchWords =  prevSearchWordsRef.current;

    useEffect(()=>{
        if(!didMountRef.current){
            Create();
            Get_Dept(props);
        }
        return() =>{
            Destroy();
        }
    // eslint-disable-next-line
    },[]);

    useEffect(()=>{
        if(!didMountRef.current){
           didMountRef.current = 1;
        }else{
            if(prevSearchWords !== searchWords){
                if(searchWords.slice(-1).split(" ").length - 1 === 0){
                    const timeout = setTimeout(() => {
                        Get_Dept(props);
                    },500)
                    return () => {
                        clearTimeout(timeout);
                    } 
                }
            }else if(prevSearchBy !== searchBy){
                if(searchWords !== ''){
                    Get_Dept(props);
                }
            }else{
                Get_Dept(props);
            }
        }// eslint-disable-next-line
    },[searchWords,searchBy,sortBy,sortType]);

    const handleSearch = (e) => {
        e.persist();
        if(searchWords==='' && e.target.name === 'searchBy'){
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                }
            });
        }else{
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                    page : 1
                }
            });
        }
    }

    return(
       <div className="container-fluid vh-100" style={{paddingTop:"60px",overflowY:"hidden"}}>
            <div className="row justify-content-between">
                    <div className="col">
                        <button 
                            className="btn btn-outline-info mr-3"
                            title ="Add a user"
                            onClick={()=>handleOnShow()}
                        >
                            <i className="fa fa-plus-circle" aria-hidden="true"></i>
                        </button>
                    </div>
            </div>
            <div className="col">
                <div className="form-inline">
                    <label htmlFor="searchInput" className="mr-3">Search By</label>
                    <SearchBy options = {['name','short_name']} selected={searchBy} onChange={handleSearch} name="searchBy"/>
                    <input 
                    type="text"
                    value ={searchWords}
                    onChange = {handleSearch} 
                    className="form-control ml-3" 
                    name="searchWords" 
                    placeholder="Search..."
                    />
                </div>
            </div>
            <div className="row mt-3">
                <div className="col">
                    <DeptTable parentprops={props}/>
                </div>
            </div>
            <DeptModal
                show={isShowModal}
                onHide={() => handleOnHide()}
                parentprops ={props}
            />
       </div>
    )
}

export const initialData = {
    isShowModal : false,
    savingLoading : false,
    depts : [],
    editDept :{
        name:'',
        short_name:'',
    },
    searchFilter : {
        searchBy : 'Name',
        searchWords : '',
        sortBy : "updated_at",
        sortType : "desc",
    },
}

const mapStatetoProps = state =>({
    isLoading : state.isLoading,
    initialData : initialData,
    data : state.data,
})

const mapDispatchtoProps = dispatch =>({ 
    Create : () => {dispatch({
        type : "CREATE",
        data : initialData
    })},
    Update : (e) => {dispatch({
        type : "UPDATE",
        name : e.name,
        value: e.value
    })},
    Update_Object : (e) =>{dispatch({
        type: "UPDATE_OBJECT",
        name :e.name,
        value : e.value
    })},
    Update_Multiple : (obj) => {dispatch({
        type : "UPDATE_MULTIPLE",
        obj : obj
    })},
    Destroy : () => {dispatch({
        type : "DESTROY",
    })},
    Create_Dept : (p)=> {dispatch({
        type : "CREATE_DEPT",
        ...p
    })},
    Get_Dept : (p)=> {dispatch({
        type : "GET_DEPT",
        ...p
    })},
    Delete_Dept : (p)=> {dispatch({
        type : "DELETE_DEPT",
        ...p
    })},
});

export default connect(mapStatetoProps,mapDispatchtoProps)(Department);
