import React,{useState,useEffect,useRef} from 'react';
import SimpleReactValidator from 'simple-react-validator';
import {toShortForm} from '../../../common/utils';

const useForceUpdate = () => {
    // eslint-disable-next-line
    const [value, setValue] = useState(0); 
    return () => setValue(value => ++value);
}

const DeptForm = (props) => {
    const {Update_Object,Create_Dept,data,initialData} =  props.parentprops;
    const {editDept} =  data;  
    const didMountRef = useRef(false);
    const prevNameRef = useRef();
    useEffect(() => {
        prevNameRef.current = editDept.name;
    });
    const prevName = prevNameRef.current;
    const validator = useRef(new SimpleReactValidator());
    const forceUpdate = useForceUpdate();
    const handleSubmit = (e) => {
        e.preventDefault();
        if (validator.current.allValid()) {
            Create_Dept({data,initialData});
            // alert("ok");
        }else {
            validator.current.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            forceUpdate();
        }   
    }

    const handleInputChanged = (e) => {
        e.persist();
        validator.current.showMessageFor(e.target.name);
        Update_Object({
            name:"editDept",
            value : {
               [e.target.name] : e.target.value
            }
        })
    }

    const handleInputBlured = (e) => {
        e.persist();
        if(e.target.value !== '' && editDept.id === undefined) {
            Update_Object({
                name:"editDept",
                value : {
                   short_name : toShortForm(e.target.value)
                }
            })
        }
    }

    useEffect(()=>{
        if(!didMountRef.current){
           didMountRef.current = true;
        }else{
            if(prevName!==editDept.name){
                if(editDept.name===''){
                    Update_Object({
                        name:"editDept",
                        value : {
                           short_name : ""
                        }
                    })
                }
            }
        }
        // eslint-disable-next-line
    },[editDept.name]);

    return(
        <div style={{maxHeight:"365px",overflowY:"auto",overflowX:"hidden"}}>
            <form id="deptForm" noValidate onSubmit={(e)=>handleSubmit(e)}>
                <div className="form-group row required">
                    <label htmlFor="name" className="col-sm-3 col-form-label control-label">Name</label>
                    <div className="col-sm-8">
                        <input 
                            type="text" 
                            className="form-control" 
                            name="name"
                            placeholder="Name"
                            value={editDept.name}
                            // ref={nameInput}
                            onChange = {(e)=>handleInputChanged(e)}
                            onBlur = {(e)=>handleInputBlured(e)}
                        />
                        <span className="text-danger">{validator.current.message('name', editDept.name, 'required|max:255')}</span>
                    </div>
                </div>
                <div className="form-group row required">
                    <label htmlFor="short_name" className="col-sm-3 col-form-label control-label">Short Name</label>
                    <div className="col-sm-8">
                        <input 
                            type="text" 
                            className="form-control" 
                            name="short_name"
                            placeholder="Short name"
                            value={editDept.short_name}
                            readOnly = {(editDept.name==='')?true:false}
                            // ref={nameInput}
                            onChange = {(e)=>handleInputChanged(e)}
                        />
                        <span className="text-danger">{validator.current.message('short_name', editDept.short_name, 'required|max:255')}</span>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default DeptForm
