import React from 'react'
import '../../System/RegisterUser/UserTable.css';
import SortColumn from '../../../common/SortColumn';

const DeptTable = (props) => {
    const {isLoading,Update_Multiple,data,Delete_Dept,Get_Dept} = props.parentprops;
    const {depts=[]} = data || {};
    const handleDelete = (id)=>{
        if(window.confirm("Are you sure to delete that user permanently?")){
            Delete_Dept({id});
        }
    }
    const refresh = () => {
        Get_Dept(props.parentprops);
    }
    const dept_rows = depts.map((val)=>{
        return  <tr key={val.id}>
            <td>{val.name}</td>
            <td>{val.short_name}</td>
            <td>
                <button 
                    className="btn btn-outline-primary mr-3"
                    title ={"Edit Department"}
                    onClick={()=> Update_Multiple({
                        "editDept":val,
                        "isShowModal":true,
                        "resetDept":val,
                        "title" : "edit"
                    })}
                >
                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </button>
                <button 
                    className="btn btn-outline-danger mr-3"
                    title ="Delete a user"
                    onClick={() =>handleDelete(val.id)}
                >
                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
    })
    return(
        <table className="table table-hover" style={{tableLayout:"fixed",width:"50%"}}>
            <thead>
                <tr>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Name" showData = {depts} />
                    </th>
                    <th scope="col" style={{width:"25%"}} className="text-center unselectabletext">
                        <SortColumn column="Short_Name" showData = {depts} />
                    </th>
                    <th scope="col" className="text-center unselectabletext">Action</th>
                </tr>
            </thead>
            <tbody style={{height:"400px"}}>
                {(isLoading)?
                    <tr style={{height:"100%"}}>
                        <td colSpan="3" align="center" style={{verticalAlign:"middle"}}>
                            <div className="spinner-border text-primary" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                            <div><h6>&nbsp;&nbsp;&nbsp;Loading ... </h6></div>
                        </td>
                    </tr>
                    :(depts.length>0)? dept_rows
                    :
                    <tr style={{height:"100%"}} onClick={()=>refresh()}>
                        <td colSpan="3" align="center" style={{verticalAlign:"middle"}}>
                            <span className="text-muted"> No data to show ... click to refresh </span>
                        </td>
                    </tr>
                }
            </tbody>
        </table>
    )
}

export default DeptTable