import React from 'react';
import DeptForm from './DeptForm'
import ModalFrame from '../../../common/ModalFrame';
import ActionBtn from '../../../common/ActionBtn';
import ResetBtn from '../../../common/ResetBtn';
const DeptModal = (props) => {
    const {parentprops,show,onHide} = props;
    const {data,Update_Multiple} = parentprops;
    const {title='save',savingLoading=false,resetDept={}} = data||{};
    let header = (title==='save')?'New Departmet' : 'Edit Department';

    const reset = () => {
        Update_Multiple({
            "editDept":resetDept
        });
    }

    return (
        <ModalFrame
            loading={(savingLoading)?1:0}
            backdrop={(savingLoading)?'static':undefined}
            size='md'
            show={show}
            onHide={() =>onHide()}
            header={header}
            form={<DeptForm parentprops={parentprops}/>}
            actionbtn = {<ActionBtn form="deptForm" loading={savingLoading} title={title}/>}
            resetbtn = {title==='edit'?<ResetBtn reset={reset} loading={savingLoading}/>:undefined}
        />
    );
}

export default DeptModal;