import React from 'react';
import {useSelector} from 'react-redux';
const Profile = () =>{
    const user = useSelector(state=>state.currentUser);
    return(
        <div className="d-flex align-items-center justify-content-center bg-light vh-100">
            <div className="d-flex flex-column">
                <div className="p-2"><h4>Profile | Detail </h4> </div>
                <div className="p-2">Name | {user.name} </div>
                <div className="p-2">Email | {user.email}</div>
                <div className="p-2">User Type | {user.type}</div>
            </div>
        </div>
    )
}

export default Profile