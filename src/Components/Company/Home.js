import React from 'react'

const Home = () =>{
    return(
        <div className="flex-center position-ref full-height">
            <div className="code">
                Home           </div>

            <div className="message" style={{padding:"10px"}}>
                Page           </div>
        </div>
    )
}

export default Home