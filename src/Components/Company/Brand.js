import React from 'react'

const Brand = () =>{
    return(
        <div className="flex-center position-ref full-height">
            <div className="code">
                About           </div>

            <div className="message" style={{padding:"10px"}}>
                Brand           </div>
        </div>
    )
}

export default Brand