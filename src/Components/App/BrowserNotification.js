import React from 'react';
import {connect} from 'react-redux';
import Notification from 'react-web-notification';

const BrowserNotification = (props) => {
    const {Update_Upper_Object} = props;
    const {bnoti_ignore,bnoti_title,bnoti_options} = props.bnoti;

    const handlePermissionGranted = ()=>{
        console.log('Permission Granted');
        Update_Upper_Object({name:"bnoti",value:{
            bnoti_ignore:false
        }})
    }
    const handlePermissionDenied = ()=>{
        console.log('Permission Denied');
        Update_Upper_Object({name:"bnoti",value:{
            bnoti_ignore:true
        }})
    }
    const handleNotSupported= () => {
        console.log('Web Notification not Supported');
        Update_Upper_Object({name:"bnoti",value:{
            bnoti_ignore:true
        }})
    }
    
    const handleNotificationOnClick = (e, tag) => {
        // console.log(e, 'Notification clicked tag:' + tag);
    }
    
    const handleNotificationOnError = (e, tag) => {
        console.log(e, 'Notification error tag:' + tag);
      }
    
    const handleNotificationOnClose = (e, tag) => {
        // console.log(e, 'Notification closed tag:' + tag);
    }
    
    const handleNotificationOnShow = (e, tag) => {
        // console.log(e, 'Notification shown tag:' + tag);
    }

    return(
        <Notification
            ignore={ bnoti_ignore && bnoti_title !== ''}
            askAgain={true}
            notSupported={handleNotSupported}
            onPermissionGranted={handlePermissionGranted}
            onPermissionDenied={handlePermissionDenied}
            onShow={handleNotificationOnShow}
            onClick={handleNotificationOnClick}
            onClose={handleNotificationOnClose}
            onError={handleNotificationOnError}
            timeout={5000}
            title={bnoti_title}
            options={bnoti_options}
        />
    )
}

const mapStatetoProps = state =>({ 
    bnoti : state.bnoti
});

const mapDispatchtoProps = dispatch =>({ 
    Update_Upper_Object: (e) => {dispatch({
        type : "UPDATE_UPPER_OBJECT",
        name : e.name,
        value: e.value
    })},
});

export default connect(mapStatetoProps,mapDispatchtoProps)(BrowserNotification)