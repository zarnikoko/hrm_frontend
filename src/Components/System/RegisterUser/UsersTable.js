import React from 'react';
import SortColumn from '../../../common/SortColumn';
import './UserTable.css';

const UsersTable = (props) => {
    const {parentprops} = props;
    const {data,isLoading,Update_Multiple,Update_Object,getUser,deleteUser} = parentprops;
    const {users=[]} = data || {};
    const handleDelete = id =>{
        if(window.confirm("Are you sure to delete that user permanently?")){
            deleteUser(
                {
                    id:id,
                    data:data,
                    Update_Object:Update_Object
                }
            );
        }
    }
    const refresh = ()=> {
        getUser(parentprops);
    }
    const usersRow = users.map((val)=>{
        val.resetPsw = false;
        return  <tr key={val.id}>
            <td>{val.name}</td>
            <td>{val.email}</td>
            <td  className="text-center">{val.type}</td>
            <td  className="text-center">
                <button 
                    className="btn btn-outline-primary mr-3"
                    title ="Edit a user"
                    onClick={()=> Update_Multiple({
                        "editUser":val,
                        "isShowRegModal":true,
                        "resetUser":val,
                        "title" : "edit"
                    })}
                >
                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </button>
                <button 
                    className="btn btn-outline-danger mr-3"
                    title ="Delete a user"
                    onClick={() => handleDelete(val.id)}
                >
                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
    })
    return(
        <table className="table table-hover">
            <thead>
            <tr>
                <th style={{width:"25%"}} className="text-center unselectabletext">
                    <SortColumn column="Name" showData = {users} />
                </th>
                <th style={{width:"25%"}} className="text-center unselectabletext">
                    <SortColumn column="Email" showData = {users} />
                </th>
                <th style={{width:"25%"}} className="text-center unselectabletext">
                    <SortColumn column="Type" showData = {users} />
                </th>
                <th style={{width:"25%"}} className="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
                {(isLoading)?
                <tr style={{height:"100%"}}>
                    <td colSpan="4" align="center" style={{verticalAlign:"middle"}}>
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                        <div><h6>&nbsp;&nbsp;&nbsp;Loading ... </h6></div>
                    </td>
                </tr>
                :(users.length>0)?usersRow
                :
                <tr style={{height:"100%"}} onClick={()=>refresh()}>
                    <td colSpan="4" align="center" style={{verticalAlign:"middle"}}>
                        <span className="text-muted"> No data to show ... click to refresh </span>
                    </td>
                </tr>
                }
            </tbody>
        </table>
    )
}

export default UsersTable
