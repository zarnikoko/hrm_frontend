import React,{useEffect,useRef} from 'react';
import UsersTable from './UsersTable';
import RegisterModal from './RegisterModal';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import {PageLimit} from '../../../common/pageLimit';
import SearchBy from './SearchBy';
const UserRegister = (props) => {
    const {data,getUser,Create,Destroy,Update_Object,Update_Multiple,User_Modal_Hide} = props;
    const {users=[],isShowRegModal=false,isRegLoading=false,searchFilter=initialData.searchFilter} = data||{};
    const {page,limit,searchBy,searchWords,sortBy,sortType,lastPage} = searchFilter;
    const didMountRef = useRef(false);
    const prevSearchByRef = useRef();
    const prevSearchWordsRef = useRef();

    useEffect(() => {
        prevSearchByRef.current = searchBy;
        prevSearchWordsRef.current = searchWords;
    });

    const prevSearchBy = prevSearchByRef.current;
    const prevSearchWords =  prevSearchWordsRef.current;

    useEffect(()=>{
        if(!didMountRef.current){
            Create();
            getUser(props);
        }
        return ()=>{
            Destroy();
        }// eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

     useEffect(()=>{
        if(!didMountRef.current){
           didMountRef.current = 1;
        }else{
            if(prevSearchWords !== searchWords){
                    if(searchWords.slice(-1).split(" ").length - 1 === 0){
                        const timeout = setTimeout(() => {
                            getUser(props);
                        },500)
                        return () => {
                            clearTimeout(timeout);
                        } 
                    }
            }else if(prevSearchBy !== searchBy){
                if(searchWords !== ''){
                    getUser(props);
                }
            }else{
                getUser(props);
            }
        }// eslint-disable-next-line
     },[page,limit,searchWords,searchBy,sortBy,sortType]);

    const handlePageClick = data =>{
        let page = data.selected+1;
        Update_Object({
            name:"searchFilter",
            value:
                {
                    page:page
                }
        });
    }

    const handleSearch = (e) => {
        e.persist();
        if(searchWords==='' && e.target.name === 'searchBy'){
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                }
            });
        }else{
            Update_Object({
                name:"searchFilter",
                value:
                {
                    [e.target.name]:e.target.value,
                    page : 1
                }
            });
        }
    }

    return(
       <div className="container-fluid vh-100" style={{paddingTop:"60px",overflowY:"hidden"}}>
           <div className="row justify-content-between">
                <div className="col">
                    <button 
                        className="btn btn-outline-success mr-3"
                        title ="Add a user"
                        onClick={()=>Update_Multiple({"isShowRegModal":true,"title":"save"})}
                    >
                        <i className="fa fa-plus-circle" aria-hidden="true"></i>
                    </button>
                </div>
                <div className="col">
                    <div className="form-inline">
                        <label htmlFor="searchInput" className="mr-3">Search By</label>
                        <SearchBy options = {['name','email']} selected={searchBy} onChange={handleSearch} name="searchBy"/>
                        <input 
                        type="text"
                        value ={searchWords}
                        onChange = {handleSearch} 
                        className="form-control ml-3" 
                        name="searchWords" 
                        placeholder="Search..."
                        />
                    </div>
                </div>
           </div>
           <div className="row mt-3">
                <div className="col">
                    <UsersTable parentprops={props}/>
                </div>
                <RegisterModal
                    show={isShowRegModal}
                    onHide={() =>User_Modal_Hide()}
                    parentprops ={props}
                />
            </div>
                <div className="row p-5">
                {(lastPage>1 && users.length!==0) ?
                    <ReactPaginate
                    key={'p1'}
                    previousLabel={'previous'}
                    previousClassName={'page-item unselectabletext'}
                    previousLinkClassName={'page-link'}
                    nextLabel={'next'}
                    nextClassName={'page-item'}
                    nextLinkClassName={'page-link unselectabletext'}
                    breakLabel={'...'}
                    breakClassName={'page-item'}
                    breakLinkClassName={'page-link'}
                    pageCount={lastPage}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    onPageChange={handlePageClick}
                    pageClassName={'page-item'} 
                    pageLinkClassName={'page-link unselectabletext noZIndex'}
                    activeClassName={'active'}
                    forcePage={page-1}
                    />
                    : null
                } 
                    <span className="mr-3"></span>
                    <PageLimit limitChange={handleSearch} currentLimit={limit} name={'limit'}/> 
                </div>
       </div>
    )
}

export const initialData ={
    users : [],
    editUser : {
        id :'',
        name :'',
        email :'',
        password : '123456',
        type :'',
        resetPsw :false,
    },
    isRegLoading : false,
    isShowRegModal :false,
    searchFilter : {
        page : 1,
        lastPage : 1,
        limit : 5,
        searchBy : 'Name',
        searchWords : '',
        sortType :'desc',
        sortBy :'updated_at'
    }
};

const mapStatetoProps = state =>({ 
    isLoading : state.isLoading,
    initialData : initialData,
    resetEditUser : initialData.editUser,
    data : state.data,
});

const mapDispatchtoProps = dispatch =>({ 
    Create : () => {dispatch({
        type : "CREATE",
        data : initialData
    })},
    Update : (e) => {dispatch({
        type : "UPDATE",
        name : e.name,
        value: e.value
    })},
    Update_Object : (e) =>{dispatch({
        type: "UPDATE_OBJECT",
        name :e.name,
        value : e.value
    })},
    Update_Multiple : (obj) => {dispatch({
        type : "UPDATE_MULTIPLE",
        obj : obj
    })},
    Destroy : () => {dispatch({
        type : "DESTROY",
    })},
    getUser : (p) => {dispatch({
        type:"GET_USER",
        payLoad:p
    })},
    registerUser : (p) => {dispatch({
        type:"REGISTER_USER",
        payLoad:p
    })},
    deleteUser : (p) => {dispatch({
        type:"DELETE_USER",
        payLoad:p
    })},
    User_Modal_Hide : () => {dispatch({
        type : "USER_MODAL_HIDE"
    })}
});

export default connect(mapStatetoProps,mapDispatchtoProps)(UserRegister);
