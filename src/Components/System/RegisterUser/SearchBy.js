import React from 'react';
import {capitalizeFirstLetter} from '../../../common/utils';

const SearchBy = props =>{
    return(
        <select className="form-control col-2" onChange={props.onChange} value={props.selected} name={props.name}>
            {
                props.options.map((val,index)=>{
                return <option key={index} value={val}>{capitalizeFirstLetter(val)}</option>
                })
            }
        </select>
    )
}

export default SearchBy