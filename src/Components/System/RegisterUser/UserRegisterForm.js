import React,{useRef,useState} from 'react';
import SimpleReactValidator from 'simple-react-validator';

const useForceUpdate = () => {
    const [value, setValue] = useState(0); 
    return () => setValue(value => ++value);
}

const UserRegisterForm = (props) => {
    const validator = useRef(new SimpleReactValidator());
    const forceUpdate = useForceUpdate();
    const {parentprops} = props;
    const {data,Update_Object,Update_Multiple,registerUser} = parentprops;
    const {editUser,resetUser,isRegLoading,title} = data;

    const nameInput = useRef(null);
    const emailInput = useRef(null);
    const typeSelect = useRef(null);

    const handleInputChanged = (e) => {
        e.persist();
        validator.current.showMessageFor(e.target.name);
        let keyname = e.target.name;
        let value = (keyname==="resetPsw")?!editUser.resetPsw:e.target.value;
        Update_Object({name:"editUser",value:{[keyname]:value}})
    } 

    const handleSubmit = (event) => {
        event.preventDefault();
        if (validator.current.allValid()) {
            registerUser({parentprops,nameInput,emailInput});
        } else {
            validator.current.showMessages();
            // rerender to show messages  for the first time
            // you can use the autoForceUpdate option to do this automatically`
            forceUpdate();
        } 

    }

    const reset = () => {
        Update_Multiple({
            "editUser":resetUser
        });
    }

    return(
        <form id="userForm" noValidate onSubmit={e => handleSubmit(e)}>
            <div className="form-group row">
                <label htmlFor="name" className="col-sm-2 col-form-label">Name</label>
                <div className="col-sm-8">
                    <input 
                        type="text" 
                        className="form-control" 
                        name="name"
                        placeholder="Name"
                        value={editUser.name}
                        ref={nameInput}
                        onChange = {(e)=>handleInputChanged(e)}
                    />
                    <span className="text-danger">{validator.current.message('name', editUser.name, 'required|max:255')}</span>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                <div className="col-sm-8">
                    <input 
                        type="email" 
                        className="form-control" 
                        name="email"
                        placeholder="Email"
                        ref={emailInput}
                        value={editUser.email}
                        onChange = {(e)=>handleInputChanged(e)}
                    />
                    <span className="text-danger">{validator.current.message('email', editUser.email, 'required|max:255')}</span>
                </div>
            </div>
            <div className="form-group row">
                <label htmlFor="type" className="col-sm-2 col-form-label">Type</label>   
                <div className="col-sm-6">         
                    <select 
                        className="form-control" 
                        onChange={(e)=>handleInputChanged(e)}
                        onBlur= {()=>validator.current.showMessageFor('type')}
                        value={editUser.type} 
                        name={'type'}
                        ref={typeSelect}
                    >
                        <option value="">Select user type</option>
                        <option value="admin">Admin</option>
                        <option value="system">System</option>
                    </select>
                    <span className="text-danger">{validator.current.message('type', editUser.type, 'required')}</span>
                </div>
            </div>  
            {(title==='edit')?
            <div className="form-group row">
                <div className="col-sm-2" style={{fontSize:"14px"}}>Reset Password</div>
                <div className="col-sm-9">
                    <div className="form-check">
                        <input 
                        className="form-check-input" 
                        type="checkbox" 
                        name={'resetPsw'}
                        checked={editUser.resetPsw}
                        onChange={(e)=>handleInputChanged(e)}/>
                    </div>
                </div>
            </div>
            :null
            }
        </form>
    )
}

export default UserRegisterForm;