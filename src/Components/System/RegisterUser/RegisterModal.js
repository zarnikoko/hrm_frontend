import React from 'react';
import UserRegisterForm from './UserRegisterForm';
import ModalFrame from '../../../common/ModalFrame';
import ActionBtn from '../../../common/ActionBtn';
import ResetBtn from '../../../common/ResetBtn';

const RegisterModal = (props) => {
    const {parentprops} = props;
    const {data,Update_Multiple} = parentprops;
    const {title='save',isRegLoading=false,editUser,resetUser} = data||{};
    let header = (title==='save')?'New User' : 'Edit User';

    const reset = () => {
        Update_Multiple({
            "editUser":resetUser
        });
    }

    return (
        <ModalFrame
            loading={(isRegLoading)?1:0}
            backdrop={(isRegLoading)?'static':undefined}
            size='lg'
            show={props.show}
            onHide={() => props.onHide()}
            header={header}
            form={<UserRegisterForm parentprops={parentprops}/>}
            actionbtn = {<ActionBtn form="userForm" loading={isRegLoading} title={title}/>}
            resetbtn = {title==='edit'?<ResetBtn reset={reset} loading={isRegLoading}/>:undefined}
        />
    );
}

export default RegisterModal;