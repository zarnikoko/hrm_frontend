import React,{useEffect} from 'react'
import {connect} from 'react-redux';
const Logout = (props) =>{

    useEffect(()=>{
        props.Logout(props);// eslint-disable-next-line
    },[]);

    const tryAgain = () =>{
       props.Logout(props);
    }

    return(
        <div className={"flex-center position-ref full-height "+(props.isLoading?"overlay":"")}>
            {(props.isLoading?
            <div>
                <div className="d-flex justify-content-center">
                    <div className="spinner-border text-primary" role="status">
                    </div>
                </div>
                <h5 className="mt-3"> Logging out ... </h5>  
            </div>
            :
            <div>
                <h5> 
                    <span className="text-danger"> Logout Failed! </span>
                    <span 
                        className="text-primary link"
                        onClick={tryAgain}
                    > 
                        Try Again 
                    </span> 
                </h5>
            </div>
            )}
            
        </div>
    )
}

const mapStateToProps = state =>({
    currentUser : state.currentUser,
    isLoading : state.isLoading
})

const mapDispatchToProps = dispatch => ({
    Logout : (p) => dispatch({
        type: 'LOGOUT',
        payLoad : p,
    })
});

export default connect(mapStateToProps,mapDispatchToProps)(Logout)