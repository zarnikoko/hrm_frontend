import React,{useEffect,useRef} from 'react';
import {Link} from 'react-router-dom';
import './Login.css';
import {connect} from 'react-redux';

const Login = (props) =>{
    const emailInput = useRef(null);
    const pwdInput = useRef(null);

    const inputChangeHandler = e => {
        e.persist();
        props.Update(e);
    };

    const handleLogin = async(e) => {
        e.preventDefault();
        const {data,history} =  props;
        props.Login({data,history,emailInput,pwdInput});
    };

    useEffect(()=>{
        props.Create();
        return ()=>{
            props.Destroy();
        }// eslint-disable-next-line
    },[]);

    return(
        <div className="main">
            <div className="login-container">
                <center>
                    <div className="middle">
                        <div id="login">
                            <form noValidate onSubmit={e=>handleLogin(e)}>
                                <fieldset className="clearfix">
                                    <p><span className="fa fa-user"></span>
                                        <input 
                                        type="text"  
                                        placeholder="Email" 
                                        required 
                                        name="email"
                                        ref={emailInput}
                                        value={props.data.email}
                                        onChange={(e)=>inputChangeHandler(e)}/>
                                    </p>
                                    <p><span className="fa fa-lock"></span>
                                        <input 
                                        type="password"  
                                        placeholder="Password" 
                                        required
                                        name="password"
                                        ref={pwdInput}
                                        value={props.data.password}
                                        onChange={(e)=>inputChangeHandler(e)}/></p>
                                    <div>
                                        <span style={{width:"48%",textAlign:"left",display:"inline-block"}}><Link className="small-text" to="/login/forgot password">Forgot
                                        password?</Link></span>
                                        <span style={{width:"50%",textAlign:"right",display:"inline-block"}}>
                                            <button> 
                                                {props.isLoading?<span><span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...</span>
                                                :
                                                  <span>Login</span>
                                                } 
                                            </button>
                                        </span>
                                    </div>
                                </fieldset>
                                <div className="clearfix"></div>
                            </form>
                            <div className="clearfix"></div>
                        </div>
                        <div className="logo">
                            LOGO
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    )
}
const mapStatetoProps = state =>({ 
    isLoading : state.isLoading,
    data : (state.data===undefined)?{
        email : '',
        password : '',
    }:state.data,
});
const mapDispatchtoProps = dispatch =>({ 
    Create : () => dispatch({
        type:"CREATE",
        data:{
            email : '',
            password:'',
        }
    }),
    Update : (e) => dispatch({
        type:"UPDATE",
        name:e.target.name,
        value:e.target.value
    }),
    Destroy :() => dispatch({
        type:"DESTROY"
    }),
    Login: p => dispatch({
        type: 'LOGIN',
        payLoad : p,
    }),
});

export default connect(mapStatetoProps,mapDispatchtoProps)(Login)