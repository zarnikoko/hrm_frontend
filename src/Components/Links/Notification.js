import React,{useState,useEffect,useRef} from 'react';
import {Link} from 'react-router-dom';
import {useSelector,connect} from 'react-redux';
import Moment from 'react-moment';
import { parseJSON } from 'jquery';

/**
 * Hook that alerts clicks outside of the passed ref
 */
// function useOutsideAlerter(ref) {
//     useEffect(() => {
//         /**
//          * Alert if clicked on outside of element
//          */
//         function handleClickOutside(event) {
//             if (ref.current && !ref.current.contains(event.target)) {
//                 alert("You clicked outside of me!");
//             }
//         }

//         // Bind the event listener
//         document.addEventListener("mousedown", handleClickOutside);
//         return () => {
//             // Unbind the event listener on clean up
//             document.removeEventListener("mousedown", handleClickOutside);
//         };
//     }, [ref]);
// }

const Notification = (props) => {
    const {Update_Upper_Object,All_Mark_As_Read,Delete_All_Read,Mark_As_Read,Delete_Read,Get_Noti_By_Scroll,Show_Notification,notiData} = props;
    const {notiLoading,notiPause,notiEnd,unreadNotiCount,readNotiCount,noti,notiPage,loadStatus} = notiData;
    const [show,setShow] = useState(false);
    const wrapperRef = useRef(null);
    const showRef = useRef(false);
    const notiDivRef = useRef();

    const readNoti = noti.filter((noti)=>{
        return noti.read_at !== null;
    });

    const unreadNoti = noti.filter((noti)=>{
        return noti.read_at === null;
    });
   
    const unreadNoti_items = unreadNoti.map((val)=>{
        return  <div className="ptr bg-success text-light p-2 mb-2 mt-2" key={val.id}>
                    <div className="row">
                        <div className="col">
                            <span style={{fontSize:"14px"}}>{parseJSON(val.data).msg}</span>
                        </div>
                        <div className="col-2 p-0 text-center">
                            <div className="btn-group-sm" role="group" aria-label="Basic example">
                                <button 
                                className="btn btn-sm btn-success" 
                                title="Mark as read"
                                onClick = {()=>Mark_As_Read(val.id)}
                                >
                                    <i className="fa fa-check" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <span style={{fontSize:"12px"}}>
                                <Moment fromNow withTitle titleFormat="MMM D, YYYY \at hh:mm:ss A UTCZ">{val.created_at}</Moment>
                            </span>
                        </div>
                    </div>
                </div>
    })
    const readNoti_items = readNoti.map((val)=>{
        return  <div className="ptr bg-secondary text-light p-2 mb-2" key={val.id}>
                    <div className="row">
                        <div className="col">
                            <span style={{fontSize:"14px"}}>{parseJSON(val.data).msg}</span>
                        </div>
                        <div className="col-2 p-0 text-center">
                            <div className="btn-group-sm" role="group" aria-label="Basic example">
                                <button 
                                className="btn btn-sm btn-secondary" 
                                title="Delete"
                                onClick = {()=>Delete_Read(val.id)}
                                >
                                    <i className="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <span style={{fontSize:"12px"}}>
                                <Moment fromNow withTitle titleFormat="MMM D, YYYY \at hh:mm:ss A UTCZ">{val.created_at}</Moment>
                            </span>
                        </div>
                    </div>
                </div>
    })

    let color =  (unreadNotiCount>0?{color:"white"}:{});

    const isBottom = el =>  {
        return el.current.scrollHeight-el.current.scrollTop === notiDivRef.current.clientHeight;
    }

    const trackScrolling = () =>{
        if(isBottom(notiDivRef)){
            if(!notiPause &&  !notiEnd && !notiLoading){
                Get_Noti_By_Scroll();
            }
        }else{
            if(notiPause){
                Update_Upper_Object({name:"notiData",value:{notiPause:false}});
            }
        }   
    }
  
    useEffect(() => {
        
        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                if(showRef.current){
                    setShow(false);
                }
            }
        }

        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);

    useEffect(()=>{
        showRef.current = show;
    },[show])

    return(
        <li className={"nav-item dropdown "+(show?"show":"")} ref={wrapperRef}>
            <Link 
            className="nav-link dropdown-toggle" 
            to="#" id="navbardrop" 
            onClick={()=>setShow(!show)}
            >
                <span>
                    <span style={{color:"red"}}>{(unreadNotiCount>0?unreadNotiCount:'')}</span>
                    <i className="fa fa-bell" aria-hidden="true" style={color}></i>
                </span>
            </Link>
            <div className={"dropdown-menu dropdown-menu-right p-0 "+(show?"show":"")} 
            style={
                {
                    maxHeight:"500px",
                    width : "400px",
                    overflowY : "auto",
                    overflowX : "hidden"
                }
            }
            id = {'header'}
            ref = {notiDivRef}
            onScroll = {trackScrolling}
            >
                {(unreadNotiCount===0 ?
                <div className="sticky-top bg-light p-3 text-center">
                    No new notification ... 
                </div>
                :
                    null
                )}
                {((unreadNotiCount>1 && loadStatus==='unread') || (readNotiCount>1 && loadStatus === 'read') || (readNotiCount>0 && loadStatus === 'unread') || (unreadNotiCount>0 && loadStatus === 'read') ?
                    <div className="sticky-top bg-light p-2">
                        {((unreadNotiCount>1 && loadStatus === 'unread')?
                            <button 
                            className="btn btn-sm btn-outline-secondary mr-3 ml-1"
                            onClick = {()=>{All_Mark_As_Read()}}
                            >
                                All mark as read
                            </button>
                        :null
                        )}
                        {((readNotiCount>1 && loadStatus === 'read')?
                            <button 
                            className="btn btn-sm btn-outline-danger mr-3"
                            onClick = {()=>{Delete_All_Read()}}
                            >
                                Delete all read
                            </button>
                        :null
                        )}
                        {((readNotiCount>0 && loadStatus === 'unread')?
                            <button 
                            className="btn btn-sm btn-outline-secondary"
                            onClick = {()=>{Show_Notification('read')}}
                            >
                                Show Read Notifications ({readNotiCount})
                            </button>
                        :null
                        )}
                        {((unreadNotiCount>0 && loadStatus === 'read')?
                            <button 
                            className="btn btn-sm btn-outline-success"
                            onClick = {()=>{Show_Notification('unread')}}
                            >
                                Show UnRead Notifications ({unreadNotiCount})
                            </button>
                        :null
                        )}
                    </div>:
                    null
                )}
                <div>
                    {unreadNoti_items}
                    {readNoti_items}
                </div>

                {(notiLoading?
                <div className="bg-light text-center">
                    <div className="spinner-grow text-info" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
                :null
                )}

                {(notiEnd && ((unreadNotiCount>5 && loadStatus === 'unread')||(readNotiCount>5 && loadStatus === 'read')) ? 
                <div className="bg-light text-center p-2">
                    No more notifications ...
                </div>
                :null
                )}

            </div>
        </li>
    )
}

const mapStatetoProps = state =>({
    notiData : state.notiData
});

const mapDispatchtoProps = dispatch =>({ 
    All_Mark_As_Read : () => {dispatch({
        type : "ALL_MARK_AS_READ",
    })},
    Delete_All_Read : () => {dispatch({
        type : "DELETE_ALL_READ",
    })},
    Mark_As_Read : (id) => {dispatch({
        type : "MARK_AS_READ",
        id : id
    })},
    Delete_Read : (id) => {dispatch({
        type : "DELETE_READ",
        id : id
    })},
    Get_Noti_By_Scroll : () => {dispatch({
        type : "GET_NOTI_BY_SCROLL",
    })}, 
    Update_Upper_Object : (p)=>{dispatch({
        type : "UPDATE_UPPER_OBJECT",
        name : p.name,
        value : p.value
    })},
    Show_Notification : (status)=>{dispatch({
        type : "SHOW_NOTIFICATION",
        status : status
    })}
});

export default connect(mapStatetoProps,mapDispatchtoProps)(Notification);