import React from 'react';
import {Link} from 'react-router-dom';
import Notification from './Notification';
import {useSelector} from 'react-redux';
import {longTextCutter} from '../../common/utils';

const LogoLink = (props) => {
    return(
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            {/* Brand */}
            <Link className="navbar-brand" to="/">Logo</Link>
            {props.children}
        </nav>
    );
}

export const SystemLink = (props) => {
    return(
        <UserLink>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" id="navbardrop" data-toggle="dropdown">
                    <span><i className="fa fa-users fa-lg" aria-hidden="true"></i>  User </span>
                </Link>
                <div className="dropdown-menu">
                    <Link className="dropdown-item" to="/user/manage">
                        <i className="fa fa-user-o" aria-hidden="true"></i> Manage
                    </Link>
                </div>
            </li>
        </UserLink>
    );
}

export const AdminLink = (props) => {
    return(
        <UserLink>
            <li className="nav-item">
                <Link className="nav-link" to="/manage_employees">
                    <span><i className="fa fa-users fa-lg" aria-hidden="true"></i> Employee</span>
                </Link>
            </li>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" id="navbardrop" data-toggle="dropdown">
                    <span><i className="fa fa-list-alt" aria-hidden="true"></i>  Leave </span>
                </Link>
                <div className="dropdown-menu">
                    <Link className="dropdown-item" to="/leave/manage">
                        <span><i className="fa fa-tasks" aria-hidden="true"></i> Manage </span>
                    </Link>
                </div>
            </li>
            <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" to="#" id="navbardrop" data-toggle="dropdown">
                    <span><i className="fa fa-building" aria-hidden="true"></i>  Department </span>
                </Link>
                <div className="dropdown-menu">
                    <Link className="dropdown-item" to="/department/manage">
                        <span><i className="fa fa-tasks" aria-hidden="true"></i> Manage </span>
                    </Link>
                </div>
            </li>
        </UserLink>
    );
}

export const GuestLink = () => {
    return(
        <LogoLink>
            {/* Links */}
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/home">
                        <span><i className="fa fa-home fa-lg" aria-hidden="true"></i> Home </span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/login">
                        <span><i className="fa fa-sign-in fa-lg" aria-hidden="true"></i> Login </span>
                    </Link>
                </li>
            </ul> 
        </LogoLink>
    )
}

const UserLink = (props) =>{
    const currentUser = useSelector(state=>state.currentUser);
    return(
        <LogoLink>
            {/* Links */}
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/dashboard">
                        <span><i className="fa fa-tachometer fa-lg" aria-hidden="true"></i> Dashboard</span>
                    </Link>
                </li>
                {props.children}
            </ul>
             {/* Dropdown */}
            <ul className="navbar-nav ml-auto">
                <Notification/>
                <li className="nav-item dropdown">
                    <Link className="nav-link dropdown-toggle" to="#" id="navbardrop" data-toggle="dropdown" title={currentUser?currentUser.name:''}>
                        Login as : {longTextCutter(currentUser?currentUser.name:'',10)}
                    </Link>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item" to="/profile">
                            <i className="fa fa-user-o" aria-hidden="true"></i> Profile
                        </Link>
                        <Link className="dropdown-item" to="/logout">
                            <i className="fa fa-user-o" aria-hidden="true"></i> Logout
                        </Link>
                        {/* <Logout/> */}
                    </div>
                </li>
            </ul> 
        </LogoLink>
    )  
}
