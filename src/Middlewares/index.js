import {checkTokenExpiration} from '../common/utils'
import {logoutUser} from '../actions/Users'

export const checkTokenExpirationMiddleware = store => next => async action => {
    const token = localStorage.getItem("usertoken");
    console.log(action);
    if(token!==null){
      if(checkTokenExpiration(token)){
        alert("Session Expired!. Please Login Again");
        localStorage.removeItem('usertoken');
        localStorage.removeItem('user');
        store.dispatch(logoutUser());
      }
    }
    console.log()
    next(action);
};

export const thunk = store => next => action =>
  typeof action === 'function'
    ? action(store.dispatch, store.getState)
    : next(action)