import React from 'react'

const ActionBtn = (props) =>{
    const {loading,form,title="save"} = props;
    return(
        <button form={form} content="Submit" type="Submit" className={"btn btn-primary"}>
                {(loading)?
                    <span>
                        <span className="spinner-border spinner-border-sm"></span> {title==='save'?'Creating...':'Updating...'}
                    </span>
                :
                    title==='save'?'Create':'Update'
                } 
        </button>
    )
}

export default ActionBtn