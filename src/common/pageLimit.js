import React from 'react';

export const PageLimit = props =>{
    const limit_options = [3,5,8,10,20];
    return(
      <select 
        className="form-control col-1" 
        onChange={props.limitChange} 
        value={props.currentLimit} 
        name={(props.name)?props.name:'limit'}
      >
        {
          limit_options.map((val,index)=>{
            return <option key={index} value={val}>{val} limit</option>
          })
        }
      </select>
    )
}