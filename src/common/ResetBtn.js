import React from 'react'

const ResetBtn = (props) =>{
    const {reset,loading} = props;
    return(
        <button 
        type="button"
        className={`btn btn-info`}
        onClick = {reset}
        disabled={(loading)?true:false}
        >
            Reset
        </button>
    )
}

export default ResetBtn