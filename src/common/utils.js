var jwtDecode = require('jwt-decode');

export const FetchMethod = (params) => {
   params.url = (params.url===undefined)?"":params.url;
   params.sendData = (params.sendData===undefined)?{}:params.sendData;
   params.isAuth = (params.isAuth===undefined)?false:params.isAuth;
   params.method = (params.method===undefined)?"POST":params.method.toUpperCase();
   params.headers = (params.headers===undefined)?{}:params.headers;

   const {url,sendData,isAuth,method,headers} = params;
   let default_headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
   };
   if(isAuth){
      default_headers.Authorization = 'Bearer '+localStorage.getItem('usertoken');
   }
   if(Object.keys(headers).length !== 0 && headers.constructor === Object){
      for (var key in headers) {
         default_headers[key] = headers[key];
     }
   }
   let default_menu = {
      method : method,
      headers : default_headers,
   } 
   if(method !=="GET"){
      default_menu.body = JSON.stringify(sendData)
   }
   return fetch(url,default_menu);
}

export const isLogin = () =>{
   if(localStorage.usertoken) return true;
   else return false ;
}

export const isJson = (str) => {
   try {
      JSON.parse(str);
   } catch (e) {
         return false;
   }
   return true;
}

export const dispRespValidationMsg = (msgJson) => {
   let disp_msg = "";
   if(isJson(msgJson)){
      msgJson = JSON.parse(msgJson);
   }
   for (var key in msgJson){
      let msg_count = msgJson[key].length;
      if(msg_count >1){
         for (var i = 0; i <msg_count; i++) {
            disp_msg = disp_msg + msgJson[key][i] + '\n';
         }
      }else{
         disp_msg = disp_msg + msgJson[key] + '\n';
      } 
   }
   return disp_msg;
}

export const longTextCutter = (text,length=20)=>{
   if(text!==null && text!== undefined && text.length > length){
      text = text.substring(0,length) + "..." ;
   }
   return text;
}

export const checkTokenExpiration = (token) => {
   if (jwtDecode(token).exp < Date.now() / 1000) {
      return true;
   }
   return false;
};

export const capitalizeFirstLetter= (string) => {
   return string.charAt(0).toUpperCase() + string.slice(1);
}

export const getTodayDate = () => {
   const today = new Date();
   const today2 = new Date(today.getTime()  - (today.getTimezoneOffset() * 60000)).toISOString().slice(0,10);
   return today2.split('-').reverse().join('-');
}

export const toShortForm = (str) => {
   if(str !=='' && str.match(/[\x00-\x7F]/)){ // This will match a single non-ASCII character: (english letter only)
      let matches = str.match(/\b(\w)/g); // ['J','S','O','N']
      str = matches.join(''); // JSON
      str = str.toUpperCase();
   }
   return str;
}

