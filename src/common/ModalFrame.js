import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
const ModalFrame = (props) => {
    return(
        <Modal
        {...props}
        size={(props.size==='md' || props.size==='lg' || props.size==='sm')?props.size:'md'}
        aria-labelledby="contained-modal-title-vcenter"
        centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {props.header}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {props.form}
            </Modal.Body>
            <Modal.Footer>
                {(props.actionbtn)?props.actionbtn:null}
                {(props.resetbtn)?props.resetbtn:null}
                <Button className="btn btn-danger" onClick={()=>props.onHide()} disabled={(props.loading)?true:false}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}
export default ModalFrame