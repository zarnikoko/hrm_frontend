import React from 'react'
import  {connect} from 'react-redux'

const SortColumn = (props) => {
    const {searchFilter={},Update_Object,column="",showData={}} =  props ;
    const {sortBy,sortType} = searchFilter;
    let count = showData.length;
    const handleSort= (sortby,sorttype)=>{
        Update_Object({name:"searchFilter",value:{"sortBy":sortby,"sortType":sorttype}});
    }
    return <span>
        {column+" "}
        {(count>1)?
            (sortBy!== column.toLowerCase())?<i className="fa fa-sort ptr" aria-hidden="true" onClick={()=>handleSort(column.toLowerCase(),'asc')}></i>:
            (sortBy === column.toLowerCase() && sortType==='asc' )?
            <i className="fa fa-sort-asc ptr" aria-hidden="true" onClick={()=>handleSort(column.toLowerCase(),'desc')}></i>
            :(sortBy ===column.toLowerCase() && sortType==='desc')?
            <i className="fa fa-sort-desc ptr" aria-hidden="true" onClick={()=>handleSort('updated_at','desc')}></i>
            :null
        :null
        }
    </span>
}

const mapStatetoProps = state=>({
    searchFilter : (state.data===undefined)?{
        sortBy :'updated_at',
        sortType : 'desc'
    }:state.data.searchFilter
});

const mapDispatchtoProps = dispatch=>({
    Update_Object :(e)=> dispatch({type:"UPDATE_OBJECT",name:e.name,value:e.value})
});
export default connect(mapStatetoProps,mapDispatchtoProps) (SortColumn)