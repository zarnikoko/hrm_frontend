import React, { useEffect} from 'react';
import {BrowserRouter as Router,Route,withRouter,Redirect,Switch} from 'react-router-dom';
import Brand from './Components/Company/Brand';
import Home from './Components/Company/Home';
import Login from './Components/Auth/Login';
import Logout from './Components/Auth/Logout';
import Dashboard from './Components/Users/Dashboard';
import Profile from './Components/Users/Profile';
import RegisterUser from './Components/System/RegisterUser'
import NotFoundPage from './Components/NotFoundPage';
import {GuestLink,SystemLink,AdminLink} from './Components/Links';
import Leaves from './Components/Admin/Leaves/index';
import Department from './Components/Admin/Department';
import {isLogin} from './common/utils';
import {PublicRoute,PrivateRoute} from './setup/routes';
import {useSelector,connect} from 'react-redux';
// import {useAlert,positions} from 'react-alert';
import BrowserNotification from './Components/App/BrowserNotification';


function App(props) {

    const {Get_Noti,Get_Noti_By_Id} = props;
    const user = useSelector(state => state.currentUser);
    // const Alert = useAlert();
    //links
    let links = null ;
    if(!isLogin()){
        links = <GuestLink/> ; 
    }else{
        if(user.type==="system"){
            links = <SystemLink/>
        }else if(user.type==="admin"){
            links = <AdminLink/>
        }
    }
    let currentDateTime = new Date().today()+" "+new Date().timeNow();
    useEffect(()=>{
            window.Echo.private('App.User.' + user.id).notification((notification) => {
                // Get_Noti();
                // Alert.info('New notification received!',{ position: positions.TOP_RIGHT });
                // const value2 = [{
                //     data : JSON.stringify(notification)
                // }]
                // Update_Upper_Object2({
                //     name : 'notiData',
                //     name2 : 'noti',
                //     value2 : value2
                // })
                // console.log(notification);
                Get_Noti_By_Id(notification);
                // Show_Browser_Notification({notification});
            });
            if(isLogin()){
                Get_Noti();
            }
        // eslint-disable-next-line
    },[]);

    return (
        <Router>
            <Route render={({ location, history }) => (
                <React.Fragment>
                    {links}
                    <div className="vh-100">
                        <Switch>
                            <PublicRoute  restricted ={false} component={Brand} path="/" exact/>
                            <PublicRoute  restricted ={false} component={Home} path="/home" exact/>
                            <PublicRoute  restricted ={true} component={Login} path="/login" exact/>
                            <PrivateRoute component={Dashboard} path="/dashboard" user_type={user.type} exact/>
                            <PrivateRoute component={Profile} path="/profile" user_type={user.type} exact/>
                            <PrivateRoute component={Logout} path="/logout" user_type={user.type} exact/>
                            <PrivateRoute component={RegisterUser} path="/user/manage" user_type="system" exact/>
                            <PrivateRoute component={Leaves} path="/leave/manage" user_type="admin" exact/>
                            <PrivateRoute component={Department} path="/department/manage" user_type="admin" exact/>
                            <Route path="/404" exact component={NotFoundPage}/>
                            <Redirect to="/404"/>
                        </Switch>
                    </div>
                    <BrowserNotification/>
                </React.Fragment>
            )}/>      
        </Router>
    );
}

const mapDispatchtoProps = dispatch =>({ 
    Get_Noti : () =>  {dispatch({
        type : "GET_NOTI",
    })},
    Get_Noti_By_Id : (noti) =>  {dispatch({
        type : "GET_NOTI_BY_ID",
        noti : noti
    })},
});

export default  connect(null,mapDispatchtoProps)(withRouter(App));
