import {takeLatest,put,call,select,delay} from 'redux-saga/effects';
import {save_leave,get_leaves,update_leave,del_leave} from '../actions/Leaves';
import {isJson,dispRespValidationMsg} from '../common/utils';
import {initialData} from '../Components/Admin/Leaves'
const store_data = (state) => state.data;

function* createLeave(){
    const data = yield select(store_data);
    console.log(data);
    const {editLeaves} = data;
    yield put({type:"UPDATE",name:"savingLoading",value:true});
    try{
        let resp;
        if(editLeaves.id === undefined){
            resp = yield call(save_leave,editLeaves);
        }else{
            resp = yield call(update_leave,editLeaves);
        }
        const resp_data = yield resp.json(); 
        if(resp_data.message){
            if(isJson(resp_data.message)){
                let message = dispRespValidationMsg(resp_data.message);
                alert(message);
            }else if(resp_data.message==='success'){
                const {update} =  resp_data;
                yield put({type:"UPDATE_MULTIPLE",obj:{'isShowModal':false}});
                yield delay(100);
                yield put({type:"UPDATE_MULTIPLE",obj:{'editLeaves':initialData.editLeaves}});
                if(update!==0){
                    yield call(getLeave,{payLoad:{data,initialData}});
                }
            }else{
                alert("Some error occured!");
            }
        }
    }catch(error){
        yield alert("Processing Failed! Error Cause : "+error);
    }
    yield put({type:"UPDATE",name:"savingLoading",value:false});
}


function* getLeave(){
    const data = yield select(store_data);
    const {searchFilter=initialData.searchFilter} = data||{};
    yield put({type:"UPDATE_UPPER",name:"isLoading",value:true});
    try{
        const resp = yield call(get_leaves,searchFilter);
        const resp_data = yield resp.json();
        if(resp_data.message){
            alert(resp_data.message);
        }
        if(resp_data.leaves){
            yield put({type:"UPDATE",name:"leaves",value:resp_data.leaves})
        }
    }catch(error){
        alert("Error Occured ! Cause : "+ error);
    }
    yield put({type:"UPDATE_UPPER",name:"isLoading",value:false});
}

function* delLeave(action){
    const {editLeaves} = action;
    let msg = window.confirm("Are you sure you want to delete?");
    if(msg === true){
        const resp = yield call(del_leave,editLeaves.id);
        const resp_data = yield resp.json();
        if(resp_data.message==='success'){
            yield call(getLeave,{payLoad:{editLeaves,initialData}});
        }else{
            alert("Some error occured!");
        }
    }
} 

function* leaveModalHide(action){
    const {name} = action;
    if(name === 'main'){
        yield put({type:"UPDATE_MULTIPLE",obj:{'isShowModal':false}})
        yield put({type:"UPDATE_MULTIPLE",obj:{'editLeaves':initialData.editLeaves}})
    }
    if(name === 'description'){
        yield put({type:"UPDATE_MULTIPLE",obj:{'isDetailModal':false}})
    }
}

export function* watchLeaveChange(){
    yield takeLatest("CREATE_LEAVE",createLeave);
    yield takeLatest("GET_LEAVE",getLeave);
    yield takeLatest("DELETE_LEAVE",delLeave);
    yield takeLatest("LEAVE_MODAL_HIDE",leaveModalHide);
}