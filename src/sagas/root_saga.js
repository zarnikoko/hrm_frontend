
import { all } from 'redux-saga/effects';
import { watchUserChange } from './user';
import {watchAuthChange} from './auth';
import {watchLeaveChange} from './leave';
import {watchDeptChange}  from './dept';
import {watchNotiChange} from './notification';

export default function* rootSaga() {
   yield all([
        watchUserChange(),
        watchAuthChange(),
        watchLeaveChange(),
        watchDeptChange(),
        watchNotiChange(),
   ]);
}

