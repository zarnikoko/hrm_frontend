import {takeLatest,put,call,delay,select} from 'redux-saga/effects';
import {get_noti,get_noti_by_id,all_mark_as_read,delete_all_read,mark_as_read,delete_read} from '../actions/Notification'

export const notiData = (state) => state.notiData;

export function* getNoti(action){
    let noti_data = yield select(notiData);
    let {loadStatus,notiPage,dateTime} = noti_data;
    try{
        const resp = yield call(get_noti,{notiPage,loadStatus,dateTime});
        const resp_data = yield resp.json();
        if(resp_data.message === 'success'){
            let value3 = {
                "unreadNotiCount" : resp_data.unreadNotiCount,
                "readNotiCount" : resp_data.readNotiCount,
            };
            if(resp_data.noti.current_page===resp_data.noti.last_page){
                value3.notiEnd = true;
            }else{
                value3.notiPage = resp_data.noti.current_page+1;
            }
            if(resp_data.noti.current_page===1){
                let currentDateTime = new Date().today()+" "+new Date().timeNow();
                value3.dateTime = currentDateTime;
            }
            yield put({
                type:"UPDATE_UPPER_OBJECT2_1",
                name:"notiData",
                name2:"noti",
                value2:resp_data.noti.data,
                value3: value3
            })
        }
    }catch(err){
        console.log(err);
    }
    
}

export function* getNotiById(action){
    const {id} = action.noti ;
    try{
        const resp = yield call(get_noti_by_id,id);
        const resp_data = yield resp.json();
        if(resp_data.message === 'success'){
            let noti_data = yield select(notiData);
            const {unreadNotiCount,loadStatus} = noti_data;
            if(loadStatus==='unread'){
                yield put({
                    type:"UPDATE_UPPER_OBJECT2",
                    name:"notiData",
                    name2:"noti",
                    value2:resp_data.noti,
                    value3:{unreadNotiCount:resp_data.unreadNotiCount}
                })
            }else{
                yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
                    unreadNotiCount: resp_data.unreadNotiCount,
                }})
            }
            yield call(showBrowserNotification,{notification:action.noti});
        }
    }catch(err){
        console.log(err);
    }
    
}

function* allMarkAsRead(){
    try{
        const resp = yield call(all_mark_as_read);
        const resp_data = yield resp.json();
        if(resp_data.message === 'success'){
            yield call(getNoti);
            yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
                noti : []
            }})
        }
    }catch(err){
        alert("Sorry try again later!");
        console.log(err);
    }
}

function* markAsRead(action){
    const {id} = action;
    let noti_data = yield select(notiData);
    let {unreadNotiCount,readNotiCount,notiEnd,notiPage} = noti_data;
    let currentDateTime = new Date().today()+" "+new Date().timeNow();
    try{
        const resp = yield call(mark_as_read,id);
        const resp_data = yield resp.json();
        if(resp_data.message === 'success'){
            let value3 = {unreadNotiCount:unreadNotiCount-1,readNotiCount:readNotiCount+1,notiPage:notiPage-1};
            yield put({
                type:"UPDATE_UPPER_OBJECT_SELF_REMOVE",
                id:id,
                name:"notiData",
                name2:"noti",
                value3: value3
            })

            yield call(getNoti);
            
        }else{
            alert("Sorry try again later!");
        }
    }catch(err){
        alert("Sorry try again later!");
        console.log(err);
    }
}

function* deleteAllRead(){
    try{
        const resp = yield call(delete_all_read);
        const resp_data = yield resp.json();
        if(resp_data.message === 'success'){
            yield call(getNoti);
            yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
                noti : []
            }})
        }else{
            alert("Sorry try again later!");
        }
    }catch(err){
        alert("Sorry try again later!");
        console.log(err);
    }
}


function* deleteRead(action){
    const {id} =  action;
    try{
        const resp = yield call(delete_read,id);
        const resp_data = yield resp.json();
        let noti_data = yield select(notiData);
        let {notiPage} = noti_data;
        if(resp_data.message === 'success'){
            yield call(getNoti);
            let value3 = {notiPage:notiPage-1};
            yield put({
                type:"UPDATE_UPPER_OBJECT_SELF_REMOVE",
                id:id,
                name:"notiData",
                name2:"noti",
                value3: value3
            })
        }else{
            alert("Sorry try again later!");
        }
    }catch(err){
        alert("Sorry try again later!");
        console.log(err);
    }
}
function* showNotification(action){
    const {status} = action ;
    let value = {
        notiLoading: true,
        notiPause:true,
        noti : [],
        notiPage : 1,
        dateTime : '',
        notiEnd : false
    };
    if(status !== ''){
        value.loadStatus = status;
    }
    yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:value})
        yield call(getNoti);
    yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
        notiLoading: false,
    }})
}
function* showBrowserNotification(action){
    const {notification} =  action;
    const {msg} = notification;

    const now = Date.now();

    const title = 'New notification receive';
    const body = msg;
    const tag = now;
    const icon = 'http://mobilusoss.github.io/react-web-notification/example/Notifications_button_24.png';
    // const icon = 'http://localhost:3000/Notifications_button_24.png';

    // Available options
    // See https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification
    const options = {
        tag: tag,
        body: body,
        icon: icon,
        lang: 'en',
        dir: 'ltr',
        sound: './sound.mp3'  // no browsers supported https://developer.mozilla.org/en/docs/Web/API/notification/sound#Browser_compatibility
    }
    yield put({type:"UPDATE_UPPER_OBJECT",name:"bnoti",value:{
        bnoti_title: title,
        bnoti_options: options
    }})
}

function* getNotiByScroll(action){
    yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
        notiLoading: true,
        notiPause:true,
    }})
        yield call(getNoti);
    yield put({type:"UPDATE_UPPER_OBJECT",name:"notiData",value:{
        notiLoading: false,
    }})
}

export function* watchNotiChange(){
    yield takeLatest("GET_NOTI",getNoti);
    yield takeLatest("GET_NOTI_BY_ID",getNotiById);
    yield takeLatest("ALL_MARK_AS_READ",allMarkAsRead);
    yield takeLatest("MARK_AS_READ",markAsRead);
    yield takeLatest("DELETE_ALL_READ",deleteAllRead);
    yield takeLatest("DELETE_READ",deleteRead);
    yield takeLatest("SHOW_NOTIFICATION",showNotification);
    yield takeLatest("SHOW_BROWSER_NOTIFICATION",showBrowserNotification);
    yield takeLatest("GET_NOTI_BY_SCROLL",getNotiByScroll);
}