import {takeLatest,put,call,delay} from 'redux-saga/effects';
import {jwtExpriedLogout} from './auth';
import {get_user,store_user,update_user,delete_user} from '../actions/Users';
import {isJson,dispRespValidationMsg}  from '../common/utils';
import {initialData} from '../Components/System/RegisterUser';

function* getUser (action){

    const {data,initialData} = action.payLoad;
    const {searchFilter=initialData.searchFilter} = data||{};

    yield put({type:"UPDATE_UPPER",name:"isLoading",value:true});

    try{
        const resp = yield call(get_user,searchFilter);
        const resp_data = yield resp.json();

        if(resp_data.message==="Unauthenticated."){

            yield call(jwtExpriedLogout,action.payLoad)

        }else if(resp_data.message==='success'){

            yield put({
                type :'UPDATE',
                name:"users",
                value:resp_data.users.data
            });

            yield put({
                type :'UPDATE_OBJECT',
                name:"searchFilter",
                value:
                    {
                        'page':resp_data.users.current_page,
                        'lastPage':resp_data.users.last_page,
                        'limit':resp_data.users.per_page,
                    }
            });

        }
    }catch(error){

        yield alert('Server not response! Please try again later'+error);
        yield put({type :'UPDATE',name:"users",value:[]})

    }
    yield put({type:"UPDATE_UPPER",name:"isLoading",value:false});
}

function* registerUser(action){

    const {parentprops,nameInput,emailInput} = action.payLoad;
    const {data,Update_Multiple,Update_Object,initialData} = parentprops;
    const {editUser,searchFilter} = data;

    yield put({type:"UPDATE",name:"isRegLoading",value:true});

    try{
        let resp ;

        if(editUser.id !== ""){

            resp = yield call(update_user,editUser);

        }else{

            resp = yield call(store_user,editUser);

        }
        const resp_data = yield resp.json();

        if(resp_data.message){

            if(isJson(resp_data.message)){

                if ('name' in JSON.parse(resp_data.message)){

                    nameInput.current.select();

                }else if('email' in JSON.parse(resp_data.message)){

                    emailInput.current.select();

                }

                let message = dispRespValidationMsg(resp_data.message);

                alert(message);

            }else if(resp_data.message==='success'){
                yield put({type:'UPDATE_MULTIPLE',obj:{'isShowRegModal':false}})
                yield delay(100);
                yield put({type:'UPDATE_MULTIPLE',obj:{'editUser':initialData.editUser}})

                const {page,searchWords} = searchFilter;
                const {count} = resp_data || 1;  //check if done update in update

                if(page === 1 && searchWords === '' && count!==0){

                    yield call(getUser,{payLoad:parentprops});

                }else if(count!==0){

                    yield Update_Object({
                        name : 'searchFilter',
                        value : {
                            page : 1,
                            searchWords : ""
                        }
                    })

                }
            }
        }  
    }catch(error){

        yield alert('Server Error! Please try again. Error cause : '+error);

    }

    yield put({type:"UPDATE",name:"isRegLoading",value:false});
}

function* deleteUser(action){

    const {id,data,Update_Object} = action.payLoad;
    const {searchFilter,users} = data;
    const {page,lastPage} = searchFilter;
    const users_count = users.length || 0;

    yield put({type:"UPDATE",name:"isRegLoading",value:true});

    try{
        const resp = yield call(delete_user,id);
        const resp_data = yield resp.json();

        if(resp_data.message==="success"){
            // yield put({type:"USERS_STORE_SUBSTRACT",id:data.success});
            if(page===lastPage && users_count===1 && page!==1){

                yield Update_Object({
                    name : 'searchFilter',
                    value : {
                        page : page-1,
                    }
                })

            }else{

                yield call(getUser,action);

            }
        }else{

            alert(resp_data.message);
            
        }
    }catch(error){

        yield alert('Server Error! Please try again. Error cause : '+error);

    }

    yield put({type:"UPDATE",name:"isRegLoading",value:false});
    
}

function* userModalHide(){
    yield put({type:"UPDATE_MULTIPLE",obj:{"isShowRegModal":false}})
    yield delay(100);
    yield put({type:"UPDATE_MULTIPLE",obj:{"editUser":initialData.editUser,"resetUser":undefined}})
}

export function* watchUserChange(){
    yield takeLatest("GET_USER",getUser);
    yield takeLatest("REGISTER_USER",registerUser);
    yield takeLatest("DELETE_USER",deleteUser);
    yield takeLatest("USER_MODAL_HIDE",userModalHide);
}