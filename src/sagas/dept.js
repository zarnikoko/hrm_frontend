import {takeLatest,put,call,select, delay} from 'redux-saga/effects';
import {jwtExpriedLogout} from './auth';
import {isJson,dispRespValidationMsg} from '../common/utils';
import {create_dept,get_dept,update_dept, delete_dept} from '../actions/Dept';
import {initialData} from '../Components/Admin/Department'
const store_data = (state) => state.data;

function* getDept(action){
    const data = yield select(store_data);
    const {searchFilter=initialData.searchFilter} = data||{};
    yield put({type:"UPDATE_UPPER",name:"isLoading",value:true});
    try{
        const resp = yield call(get_dept,searchFilter);
        const resp_data = yield resp.json();
        if(resp_data.message==="Unauthenticated."){
            yield call(jwtExpriedLogout,action.payLoad)
        }else if(resp_data.message==='success'){
            yield put({
                type :'UPDATE',
                name:"depts",
                value:resp_data.depts
            });
        }else{
            alert("Error in loading departement data. Error cause : "+resp_data.message);
        }
    }catch(error){
        yield alert('Server not response! Please try again later'+error);
        yield put({type :'UPDATE',name:"depts",value:[]});
    }
    yield put({type:"UPDATE_UPPER",name:"isLoading",value:false});
}

function* createDept(action){
    const data = yield select(store_data);
    const {editDept} = data;
    yield put({type:"UPDATE",name:"savingLoading",value:true});
    try{
        let resp;
        if(editDept.id===undefined){
            resp = yield call(create_dept,editDept);
        }else{
            resp = yield call(update_dept,editDept);
        }
        const resp_data = yield resp.json(); 
        if(resp_data.message){
            if(isJson(resp_data.message)){
                let message = dispRespValidationMsg(resp_data.message);
                alert(message);
            }else if(resp_data.message==='success'){
                const {count} =  resp_data;
                yield put({type:"UPDATE_MULTIPLE",obj:{'isShowModal':false}});
                yield delay(100);
                yield put({type:"UPDATE_MULTIPLE",obj:{'editDept':initialData['editDept']}});
                if(count!==0){
                    yield call(getDept);
                }
            }else{
                alert("Some error occured!");
            }
        }
    }catch(error){
        yield alert("Processing Failed! Error Cause : "+error);
    }
    yield put({type:"UPDATE",name:"savingLoading",value:false});
}

function* deleteDept(action){
    const {id} =  action;
    const resp = yield call(delete_dept,id);
    const resp_data = yield resp.json();
    if(resp_data.message==='success'){
        yield call(getDept);
    }else{
        alert("Some error occured!");
    }
}

export function* watchDeptChange(){
    yield takeLatest("CREATE_DEPT",createDept);
    yield takeLatest("GET_DEPT",getDept);
    yield takeLatest("DELETE_DEPT",deleteDept);
}