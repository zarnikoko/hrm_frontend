import {takeLatest,put,call, delay} from 'redux-saga/effects';
import {login,logout} from '../actions/Auth';
import {isJson,dispRespValidationMsg}  from '../common/utils';
import {InitialState} from '../reducers'

function* Login(action){

    const {data,emailInput,pwdInput} = action.payLoad;

    yield put({type:"UPDATE_UPPER",name:"isLoading",value:true});

    try{
        const resp = yield call(login,data);
        const resp_data = yield resp.json();

        let message;
        if(isJson(resp_data.message)){   // login fail

            message = JSON.parse(resp_data.message);

            if ('email' in message){

                emailInput.current.select();

            }else if('password' in message){

                pwdInput.current.select();

            }

            message = dispRespValidationMsg(resp_data.message);
            alert(message);

        }else{ 
            if(resp_data.user){
                const {token,user} = resp_data;
                yield localStorage.setItem("usertoken", token);

                yield localStorage.setItem('user',JSON.stringify(user));

                yield put({type:"UPDATE_UPPER",name:"currentUser",value:resp_data.user});

                window.location.reload();
                // history.push('/profile');

            }else{
                alert(resp_data.message);
                // alert("Login Failed! ...")
            }
        }
    }catch(error){

        yield alert('Server not response! Please try again later'+error);

    }

    yield put({type:"UPDATE_UPPER",name:"isLoading",value:false});

}

function* Logout(action){

    yield put({type:"UPDATE_UPPER",name:"isLoading",value:true});

    try{

        yield call(logout);

        yield call(removeUserAndToken,{payLoad:action.payLoad});

    }catch(error){

        yield alert('Server Error! Please try again. Error cause : '+error);

    }

    yield put({type:"UPDATE_UPPER",name:"isLoading",value:false});
}

export function* jwtExpriedLogout(action){

    yield alert("Every 8 hours , Session will expired, you need to sign in again to continue!");
    yield call(removeUserAndToken,{payLoad:action});

}

function* removeUserAndToken(action){

    const {history,currentUser} = action.payLoad;
    const {id} = currentUser;
    yield localStorage.removeItem('usertoken');
    yield localStorage.removeItem('user');
    yield put({
        type:"UPDATE_UPPER_MULTIPLE",
        obj: {
                ...InitialState,
                currentUser : {}
            }
    });
    yield window.Echo.leave('private-App.User.'+id);
    history.push('/login');
    
} 

export function* watchAuthChange(){
    yield takeLatest("LOGIN",Login);
    yield takeLatest("LOGOUT",Logout);
}