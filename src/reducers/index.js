export const InitialState = {
    currentUser : (localStorage.user===undefined)?{}:JSON.parse(localStorage.user),
    isLoading : false,
    notiData :{
        notiLoading : false,
        notiPause : false,
        notiEnd : false,
        unreadNotiCount : 0,
        readNotiCount : 0,
        loadStatus : 'unread',
        noti : [],
        notiPage : 1,
        dateTime : '',
    },
    bnoti : {
        bnoti_ignore : true,
        bnoti_title : '',
        bnoti_options :{},
    }
    // currentPage : 1,
    // lastPage : 1,
    // limit : "5",
}
export  const reducer = (state = InitialState, action) =>{
    switch(action.type){
        case 'CREATE':
            return {
                ...state,
                data : action.data
            };
        case 'UPDATE_UPPER' :
            return {...state,[action.name] : action.value};
        case 'UPDATE_UPPER_OBJECT' :
                return {
                    ...state,
                    [action.name] : {
                        ...state[action.name],
                        ...action.value
                    }
                };
        case 'UPDATE_UPPER_OBJECT2' :
            // const joined2 = state[action.name][action.name2].concat(action.value2);
            const joined2 = [action.value2].concat(state[action.name][action.name2]);
            return {
                ...state,
                [action.name] : {
                    ...state[action.name],
                    [action.name2]:joined2,
                    ...action.value3
                }
            };
        case 'UPDATE_UPPER_OBJECT2_1' :
            let obj = state[action.name][action.name2].concat(action.value2);
            let joined = obj.filter( (ele, ind) => ind === obj.findIndex( elem => elem.id === ele.id));
            return {
                ...state,
                [action.name] : {
                    ...state[action.name],
                    [action.name2]:joined,
                    ...action.value3
                }
            };
        case 'UPDATE_UPPER_OBJECT_SELF' :
            let text = action.value2;
            let update_obj = state[action.name][action.name2].map(el => (el.id === action.id ? {...el,...text} : el))
            return {
                ...state,
                [action.name] : {
                    ...state[action.name],
                    [action.name2]:update_obj,
                    ...action.value3
                }
            };
        case 'UPDATE_UPPER_OBJECT_SELF_REMOVE' :
            let removed_obj = state[action.name][action.name2].filter(obj => obj.id !== action.id)
            return {
                ...state,
                [action.name] : {
                    ...state[action.name],
                    [action.name2]:removed_obj,
                    ...action.value3
                }
            };
        case 'UPDATE_UPPER_MULTIPLE' :
            return {...state,...action.obj};
        case 'UPDATE':
            return{
                ...state,
                data:{
                    ...state.data,
                    [action.name] : action.value
                }
            }
        case 'UPDATE_OBJECT':
            return{
                ...state,
                data:{
                    ...state.data,
                    [action.name] :{
                        ...state['data'][action.name],
                        ...action.value
                    }
                }
            }
        case 'UPDATE_MULTIPLE' :
            return{
                ...state,
                data:{
                    ...state.data,
                    ...action.obj
                }
            }
        case 'DESTROY':
            return {
                ...state,
                data:undefined
            }
        // case 'USERS_STORE' : 
        //     return {...state,users:action.data};
        // case 'USERS_STORE_FAIL' : 
        //     return {...state,users:action.data};
        // case 'USERS_STORE_ADD' : 
        //     const joined = state.users.concat(action.data);
        //     return {...state,users:joined};
        // case 'USERS_STORE_UPDATE' : 
        //     var index = state.users.findIndex(u=> u._id === action.data._id);
        //     return {
        //         ...state,
        //         users:[
        //         ...state.users.slice(0,index),
        //         Object.assign({}, state.users[index], action.data),
        //         ...state.users.slice(index+1)
        //         ]
        //     }
        // case 'USERS_STORE_SUBSTRACT' : 
        //     const newArray = state.users.filter(obj => obj._id !== action.id);
        //     return {...state,users:newArray};
        // case 'SET_REGISTER_LOADING' : 
        //     return {...state,isRegisterLoading:action.status};
        // case 'SHOW_REGISTER_MODAL' : 
        //     return {...state,isShowRegisterModal:action.status,editUserInfo:action.data};
        // case 'SET_REGISTER' : 
        //     return {
        //         ...state,
        //         isShowRegisterModal:action.status,
        //         isRegisterLoading:action.status,
        //         users : (action.status!== undefined)?[]:action.status,
        //         searchBy : (action.status !== undefined) ? 'Name' : action.status,
        //         searchWords : (action.status !== undefined) ? '' : action.status,
        //     };
        // case 'PAGE_CHANGE':
        //     return {
        //         ...state,
        //         currentPage : action.page
        //     };
        // case 'SEARCH_IN_USER_REGISTER':
        //     return {
        //         ...state,
        //         [action.search.name] : action.search.value,
        //         currentPage : action.search.page
        //     };
        case 'SET_LEAVE_STORE' : 
            return {
                ...state,
                isShowModal :action.set,
                isShowDtlModal : action.set,
                isEditModal : action.set,
                isRegisterLoading:action.set,
                leaves:(action.status!== undefined)?action.status:[],
                name:(action.set===undefined)?action.set:'',
                numbersOfDay:(action.set===undefined)?action.set:'',
                description:(action.set===undefined)?action.set:''
            };
        // case 'GET_LEAVES' :
        //     return{
                
        //     }
        case 'INPUT_CHANGE' : 
            return {
                ...state,
               [action.event.target.name] : action.event.target.value
            };
        case 'SAVE_STORE' : 
            return {
                ...state,
               [action.name] : action.value
            };
        case 'EDIT_INPUT_CHANGE' :
            return {
                ...state,
                [action.e.target.name] : action.e.target.value
            }

        case 'SHOW_MODAL' : 
            return {...state,isShowModal:action.status};
        case 'CLICK_LEAVE_DEC_DETAIL':
            return {
                ...state,
                isShowDtlModal : action.modalState,// modalState ka true or false close yin  lal d reducr pal use loz ya ag a shin htr tr hu
                clickLeave : action.value,// click tae leave yae value ae tak tabale mhr ae reducer fun nae value u mal
            };
        case 'EDIT_MODAL' :
            return { 
                ...state,
                [action.name] : action.value
            }
        case 'EDIT_FORM':
            return{
                ...state,
                isEditModal : action.modalState,
                editData : action.value
            }
        default : 
            return state;
    }
}